## Développeur data stagiaire au sein de TEKLIA en 2021
<p style="text-align:justify;">
Conception d'une application Python générant des fichiers pdf et XML Alto, à partir de documents numérisés et soumis à un outil de reconnaissance d'écriture manuscrite.
</p>

* Conception d'une base de données à partir d'un export de la base de données ArkIndex de TEKLIA
* Exploitation de la base de données pour inclure les informations issues de la reconnaissance d'écriture dans les fichiers pdf et XML Alto (polygônes, bounding box et chaînes de caractères).
* Tests unitaires et tests d'intégration utilisant fixture et mock
* Intégration sous forme de console script avec prise en charge d'argument en ligne de commande
* Intégration et déploiement continu via Gitlab
L'application génére des fichiers indexables qui permettent de sélectionner et rechercher du texte sur un document manuscrit.

[Accéder au rapport de stage](https://drive.google.com/file/d/1itXArWib1jMBT4AVfzh1UBlQ0WA0x4F1/view?usp=sharing)


## Développeur data dans un projet en collaboration avec La Turbine et TEKLIA en 2021
<p style="text-align:justify;">
Conception d'une application Python responsable du pipeline ETL ainsi que de l'analyse des données:
</p>

* Génération d'une base de données SQLite embarquée, construite à partir du parsing des rapports Kaldi fournis par TEKLIA,
* Requêtage et edition des rapports conrrespondants sous forme de fichier HTML, JSON et CSV
* Intégration de l'application sous forme de console script avec prise en charge d'argument en ligne de commande
* Tests unitaires utilisant fixture et mock
* Déploiement continu via Gitlab
Le projet a été mise en place en respectant les principes et les cérémonies de la Méthode Agile, supervisé par la Turbine.  
Le code proposé a été réinvesti par TEKLIA pour concevoir une application open-source.

[Accéder au code-source de l'application](https://gitlab.com/teklia/kaldi-analyzer)


## Professeur de Mathématiques dans la région grenobloise de 2015 à 2020

* Cours magistraux pour tous les niveaux de la 6ème au BTS, fillières générales,
professionnelles et technologiques,
* Séances d'aide personnalisée en filière scientifique sur le thème des algorithmes
d'optimisations : Mise en œuvre de la théorie sur le calcul différentiel pour la recherche
d'extremum, implémentation des algorithmes sous TI-Basic,
* Séances d'aide personnalisée en filière professionnelles sur le thème de la
programmation; implémentation des algorithmes en Python,
* Séances d'aide personnalisée en fillière économique sur le thème de la modélisation
mathématiques en économie; implémentation des modèles en utilisant les tableurs,
* Travaux pratiques en lycée générale sur le thème des probabilités et de la cryptographie ;
implémentation des modèles en utilisant les tableurs.


## Actuaire conseil au sein d'Aquila Risk Solutions au Luxembourg de 2011 à 2012

* Production du rapport trimestriel d’une assurance-vie à destination du CAA :
Mise à jour des documents comptables, analyse de la marge de solvabilité.
* Production du rapport d’actuaire d’une compagnie d’assurance-vie à destination du
CAA :
Analyse des comptes, modélisation et provisionnement d’une garantie d’investissement,
réévaluation du provisionnement d’une couverture invalidité, rédaction du rapport
d’actuaire.
* Rapport d’actuaire d’une compagnie d’assurance non-vie à destination du CAA:
Analyse des comptes, stress-test des obligations et des fonds monétaires,
Rédaction du rapport d’actuaire.
* Réévaluation de la provision d’équilibrage-crédit d’une compagnie d’assurance non-vie.
* Etude des spécifications techniques de niveau 1 et 2 de Solvabilité II.
* Réplication du QIS 5 d’une compagnie d’assurance non-vie.
