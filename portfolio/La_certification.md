<p style="text-align:justify;">

Afin de couvrir le plus large spectre de compétences lors du passage de ma certification, j'ai choisi de reprendre un projet réalisé pendant la formation. Le projet a été remanié; le code réécrit intégralement sous Python, et la structure de la base de données repensée pour bénéficier d'optimisations.

</p>

* [Mon rapport de certification](https://drive.google.com/file/d/1iarKrowtKsZqg8S1DHqB3K18EJu6MJ5N/view?usp=sharing)