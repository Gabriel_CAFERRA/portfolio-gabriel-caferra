---
hide:
  - navigation
---

![Gre_at_night!](./static/night_bastille.jpg)

## Développeur Data
<p style="text-align:justify;">

Mes centres d'intérêts professionnels actuels incluent la création et la gestion de base de données, la conception et l'administration des systèmes.  
  
</p>

## Simplon Grenoble

<p style="text-align:justify;">  

Désireux de faire évoluer ma carrière, j'ai suivi la formation Developpeur Data à Simplon Grenoble dont j'ai validé les trois certifications. Les membres de notre promotion ont des parcours bien différents mais nous sommes tous réunis autour d'un intérêt commun: la data! En remote ou présenciel, en autonomie ou en équipe, nous construisons des projets inspirés par des cas concrets, avec les outils du monde professionnel et ses méthodes agiles. 

</p>

## Ecole IA de Simplon Grenoble

<p style="text-align:justify;">  

Ma formation de Développeur Data n'a fait que confirmer mon intérêt pour toutes les problématiques liées à la data. Le Big Data et l'Intelligence Artificielle constitue un univers à part entière dans le monde de la data. J'ai donc postulé à l'Ecole IA de Simplon Grenoble pour découvrir ces disciplines d'avenir. Conscient de la qualité de la formation proposée, je souhaite devenir un collaborateur privilégié des experts du Machine Learning, dans sa mise en application et son déploiement.

</p>
---

[Télécharger mon CV](https://drive.google.com/file/d/1RzBxHSLA_eCdHLiBmuRGPVZBRraDYseH/view?usp=sharing){target=_blank}

<!---
## Projets

Retrouvez mes projets sur [:fontawesome-brands-gitlab: GitLab](https://gitlab.com/?personal=true&sort=latest_activity_desc).

### [Job Dashboard](https://gitlab.com/Gabriel_CAFERRA/projet_3){target=_blank}

> Février 2021
<p style="text-align:justify;">

Simplon cherche à comprendre les spécificités de l'emploi dans le secteur de la Data, afin de pouvoir guider au mieux les apprenants dans leur insertion professionnelle.

</p>
<p style="text-align:justify;">

Il a été décidé qu'une analyse journalière des offres d'emploi en ligne est un bon point de départ pour obtenir des données concrètes et à jour.

</p>
<p style="text-align:justify;">

Simplon souhaite obtenir un système permettant de récupérer et d'analyser automatiquement les offres d'emploi dans le secteur de la Data, avec un tableau de bord présentant des métriques métier intéressantes sous la forme d'une page web accessible en ligne.

</p>

*Compétences* :  
Extraction de données (Scraping, flux RSS/Atom)  
Déploiement (GitLab Pages)  
Gestion de projet en mode Agile

*Technologies* :  
PostgreSQL, Python (environnement virtuel), SQL, Bash

### [Suivi de la pollution atmosphérique](https://gitlab.com/Gabriel_CAFERRA/projet_2-groupe_6/-/tree/projet_2){target=_blank}

> Janvier-Février 2021
<p style="text-align:justify;">
Les seuils fixés par l'Europe pour les particules PM10 et le dioxyde d'azote NO2 doivent être respectés sous peine de lourdes amendes.
</p>
<p style="text-align:justify;">
En Auvergne-Rhône-Alpes, différentes zones sont concernées par ces dépassements. Noux avons donc besoin de différents outils pour suivre et analyser la pollution atmosphérique à l'échelle de la région.
</p>

Trois besoins ont été retenus :

* Avoir un tableau de bord avec différents indicateurs globaux (échelle de la région) concernant la qualité de l'air.  

* Avoir une cartographie de la région avec les différents polluants atmosphériques mise à jour toutes les heures.

* Avoir un notebook Jupyter qui contienne toutes les explications pour faire des requêtes sur la base de données ainsi qu'une cartographie. Le but est que les data scientists soient très vite opérationnels sur les données.

* Connaître l'impact du confinement sur la pollution atmosphérique dans la région.

*Compétences* :  
Développement web  
Intégration continue  
Déploiement continu  
Gestion de projet

*Technologies* :  
Git, Bash, SQL, PostgreSQL, Python, Metabase

### [Mesure d'audience Web](https://gitlab.com/Gabriel_CAFERRA/projet_1-groupe_2/-/tree/Gabriel-CAFERRA){target=_blank}

> Janvier 2021
<p style="text-align:justify;">
Nous avons récupéré les logs d'un serveur d'un site-web de e-commerce. Nous souhaitons pouvoir effectuer une mesure d'audience des visiteurs.
</p>

Nous avons pu répondre les questions suivantes :

* Quel est le traffic sur le site ?

* Combien de personnes visitent le site ?

* Quels sont nos produits les plus et moins intéressants pour notre clientèle ?

* De quel endroit d'internet viennent les visiteurs avant d'arriver sur le site ?

* Quels moments de la journée sont les plus/moins propices à la vente ?

* Quels navigateurs sont les plus / moins utilisés par les visiteurs ?

* Quels systèmes d'exploitation sont les plus / moins utilisés par les visiteurs ?

* Quels appareils sont les plus / moins utilisés par les visiteurs ?

*Compétences* :  
Conception, structuration, nettoyage de base de données relationnelle,  
Conception et réalisation d'un rendu visuel des données issues du processus d'extraction

*Technologies* :  
Git, Bash, SQL, SQLite, Python, Metabase

## Formations

### [Simplon Développeur Data](https://simplon.co/formation/specialisation-analyste-data/487){target=_blank}

> Novembre 2020 - Septembre 2021, Grenoble

<p style="text-align:justify;">

De l’analyse du besoin à la data visualisation, en passant par la récolte et le traitement des données, le·la développeur·se data conçoit et exploite les bases de données.
</p>
<p style="text-align:justify;">

Le·la développeur·se data gère l'ensemble du cycle de vie de la donnée, de la donnée brute jusqu'à la livraison de données utilisables. Il s’agit d'un·e technicien·ne capable d'appréhender n'importe quel type de format de données, de les stocker en base de données, les interroger et de les servir, avec un rendu visuel ou un support adapté pour un usage tiers. Il·elle peut être amené·e à automatiser des processus d'acquisition, d'import, d'extraction et de visualisation de données. Il·elle est le garant de la qualité, de l'intégrité et de la cohérence des données avant et après traitement.
</p>

Le métier de développeur·se data s’articule autour de 3 compétences que j'ai validées à l'issue de la formation :

* [Exploiter une  base de donnée](https://www.francecompetences.fr/recherche/rs/3508/)

* [Développer une base de données](https://www.francecompetences.fr/recherche/rs/3497/)

* [Méthodes agiles et amorçage de projets](https://www.francecompetences.fr/recherche/rs/2085/)

### [MASTER professionnel en Sciences Actuarielle et Financière ISFA](https://isfa.univ-lyon1.fr/cursus-formation-actuaire-a-lisfa){target=_blank}

> Septembre 2006 - Septembre 2008

<p style="text-align:justify;"> 

L'actuaire est un spécialiste de l'application des probabilités, des statistiques et de la gestion des risques. Il oeuvre dans les domaines de l'assurance, la finance et la protection sociale en apportant des réponses concrètes en termes de valorisation de portefeuilles, tarification, modélisation des risques, mesure de solvabilité, ...

</p>
--->
