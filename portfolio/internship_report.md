# Introduction

## An AI software company

Teklia has been evolving in the French AI (Artificial Intelligence) software engineering for 6 years. Its main purpose is helping productivity gain through Document Processing software solutions. Core technology focus on Machine Learning, Deep Learning and Natural Language Processing (NLP). Thus, Telkia’s main area of expertise is the automatic recognition of handwritten and printed documents in any format or language and from any period of time. Among processed documents are medieval calligraphied religious text, handwritten parish registers and much more (demander d'autres exemples). Worldwide recognized know-how serves a large panel of collaborators from universities, ministries and research centers to insurance or cosmetic companies. It means a strong capacity to adapt to a wide range of activity domains and interlocutors. 

## Team TEKLIA

The Telkia team is composed of AI experts and developers that build custom solutions. Projects goal is to deliver an intuitive and easy-to-use software adapted to customers business needs. Each team member is passionated with software conception and development, AI applied to concrete issues. By the way, Teklia has its own blog to share technical and popularized articles around these expertise areas. 

rajouter le lien

## Solutions overview

Typical customers data pipeline would be:  
* Scan and Import: import any kind of documents or any file formats, from scanned images to Word or PDF documents,
* Read: data capture solution can read any font or handwritten script in multiple languages (French, English, Arabic, Chinese, …), thanks to Optical Character Recognition (OCR) or Handwritten Text Recognition (HTR),
* Analyze: AI tools understand the structure of customers documents (titles, section, tables or figures) and analyzes the data according to their needs,
* Extract and classify: software uses intelligent data extraction to automate the doucments classification, indexation and archiving.

TEKLIA put to use several tools for their customers, based on the steps ahead.  

### OCAPI - Form protection

OCAPI is the French acronym for "Outil de Captcha et d'Annotation du Patrimoine en Image" or "Other Captcha to Annotate Patrimonial Images" in English.
Ocapi objective, as a captcha, is to ensure that the user filling in the form on a website is a human user, and not a robot trying to validate the form automatically. Novelty of Ocapi is to enhance the value of digitized cultural heritage content and to involve Internet users in the validation of indexed or OCRized corpora in an automatic or collaborative way.
Ocapi challenges user with validating transcriptions, image classification or audio recognition. Learn more about Ocapi:https://docs.ocapi.teklia.com/index.html

### Ocelus - Machine Learning as API

OCELUS is a handwritten document recognition web service easily accessible through a simple web API. It currently segments and extracts handwritten documents, such as cover letters, subscription forms, satisfaction polls, purchase orders, medical records. Text segmentation and recognition identifies words, numbers, sentences and paragraphs. It transcribes them into electronic text, in French and English (Arabic is to come). The API allows model customization for specific documents, it will read text according to a pre-determined structure and type of writing. For example, it is particularly useful to retrieve data from subscription forms or purchase orders that often are half-typed half-handwritten documents. [Learn more about OCELUS](https://teklia.com/solutions/ocelus/)

### Arkindex - TEKLIA Document processing platform

We will put a special focus on Arkindex as it is the core of the project purpose. Understanding what is Arkindex, is necessary for the following steps.  
Arkindex gather all TEKLIA know-hows in one API, starting from Extract Transform and Load (ETL) pipeline, further advanced AI analysis and automatic database update.  
From a user point of view, Arkindex allows to:

 
* import and organize images of document from files or manifests that automate files importation from a website.  
* manually annotate images with zones, transcriptions and classifications:
through the annotation interface, users can:
- define zones on the images with rectangles or polygones
- define the type and class of the zones, such as images, paragraphs or textline
- add manual transcription for the text in the selected zone
![Arkindex interface](./arkindexinterface.png)
Alongside user data management, Arkindex applies machine learning automatons called workers

#### Arkindex workers
Basically, a worker is a container with a python script inside. The script will call for machine learning librairies such as keras, tensorflow, scikit-learn, kaldi or dhSegment. These librairies load machine learning models each one responsible for:

* document layout analysis (DLA) that automatically detect documents components (texts, images, graphics, etc)
* a printed (OCR) and handwritten text recognition (HTR)
* named entity recognition that automatically extracts named-entity: identify persons, places, organizations, etc.  

#### Next step for Arkindex
Considering current Arkindex structure, next step would be to automatically create a new machine learning model:
From imported data, the model can already be annotated by humans. Then machine learning model could be trained and validated.

Best way to fully discover Arkindex is to [try it!]:(https://demo.arkindex.org/)
Arkindex does not require you to log in, however authenticated users can access most of the main features.

# Part one - a challenging project :
    
## Understanding TEKLIA needs on arkindex platform

On first approach, Arkindex users are willing to: 
* get an on the fly document with the most recent transcription, from their scanned documents into a numeric format such as a pdf document,
* the generated file must contain the whole orginal document or a selected part of it, showing the original scanned images,
* transcriptions from Arkindex database must be indexed in the generated document, so that users would be able to search the generated document as they do in a pdf document. Search results would display relevant section in the scanned image, highlighting the transcription found by the HTR tool. Here is an example from a test purpose pdf. Users was looking for the character string "suite of products". The embedded pdf searcher have highlighted the relative characters on the pdf document.  

![pdf sample](./pdfdemo.png)

In addition:
* other formats than pdf should be available, for customers or TEKLIA needs, such as TEI-XML files,
* document generation must be of easy use, from a console script or directly trigerred from Arkindex platform,
* a debug mode has to be selectable, showing transcription text from Arkindex database for further debug or analysis
* the script must accept several command line interface (CLI) arguments. As element type names may vary from one corpus to another, end-user should have the possibility to specify the name of folder type when launching the script. 
* the same approach is to be followed with document generation, a CLI argument must specify if pdf or another format is wanted.

It implies relevant data from ARKINDEX database has to be exported first, in order to be processed with a document generation tool.

## State of the art

As the documents are to be generated from the Arkindex platform data and SQLite database export is available, we will discuss document generation with the Python language and the packages available to do so. Main purpose will be to explain the choice to use reportlab package. Firstly, let's mention some other document generation libraries commonly used by Python community.

### PyPDF4

A pure-Python coded package that you can use for many different types of PDF operations. However, PyPDF is interesting when retrieving data from existing pdf document. PyPDF can get data from a pdf or handle pdf elements such as text, number of pages or rotate and merge pages. On the other hand, PyPDF won't be able to generate a pdf, inserting textline or rotating images before editing. Learn more about PyPDF4:

### PyFPDF

This library offers no external dependencies or extensions (optionally PIL for GIF support), but the use is only for page layout and object insertions. Advance handling for texts or figures relies on extra packages such matplotlib or pylab. As itself PyFPDF does not allow text rotation, figures translations.Learn more about PyFPDF:

Despite their popularity, these previous libraries won't do the job as no further customisable edition is implemented, here is why reportlab is the best candidate for this project...

### The main features of Reportlab - what to keep in mind

ReportLab has been around for a very long time among the most popular options for generating PDFs in Python programming language. Its longevity is explained by its open source version that ensures a continuous update.
Part of the reportlab is based on the PIL library which has eased compatibility with Python newer versions. Nevertheless, besides 
Full code coverage for editing instructions ensure complete control over pdf generation:  

* Customizable document template for automatic page layout and text edition.  
* A large panel of geometric actions allows easy switching between text, image and figure drawing,
* Report Markup Language (RML) compatibility for advanced style layout (for premium version)
Finally, all these functionalities are largely discussed across the forums, blogs, the [official website](https://www.reportlab.com/) and relative [user guide](https://www.reportlab.com/docs/reportlab-userguide.pdf)

Nevertheless, besides these advantages, it has to be kept in mind that alongside their complementarity, reportlab and PIL do not share an harmonized base:

* Image module from PIL is different from same-named module from reporlab. 
* reportlab coordinates system starts from bottom-left corner when PIL coordinates system starts from top-left. It can be explained by the different use of these two librairies. PIL is for image handling in Python whereas Reportlab focuses on page edition.
From a larger point of view, make sure to get the proper distribution depending on which operating system is used. It can be problematic when distributing the application across multiple platforms.


<table align="center" border="1" cellpadding="4" cellspacing="0">
<thead><tr><th align="center"> pros</th><th align="center">cons</th></tr></thead>
<tbody>
<tr valign="top"><td>*Here* and<br><strong>there</strong></td><td align="right">1,234,567</td></tr>
<tr valign="top"><td>Everywhere</td><td align="right">2</td></tr>
</tbody>
<tfoot><tr valign="top"><td align="right">Sum:</td><td align="right">1,234,569</td></tr>
</tfoot>
</table>

### XML generation

Arkindex exports produce a lot of information that cannot be exploited when generating a pdf as the final document is static. The visible character string are no longer related to a certain text block with a bounding box polygon. An alternative to keep all necessary layout data for reusability and sustainability would be to generate an XML file. XML file allows to define a custom markup hierarchy and with relative attributes for each attribute. Therefore, we can create a markup for a textblock with a polygon attribute and a new subelement markup with the string contained in. However, this freedom in design has to be counterbalance by final user needs. Thereby, a lot of standardized xml formats have been designed. We will now discuss on two standard formats that are commonly used when dealing with OCR and HTR.
 
#### TEI-XML

TEI-XML actors are the academic sectors, libraries and archives as main TEI-XML format intention is the encoding of cultural heritage texts.
TEI-XML focuses on adding an understanding of the document content besides the descritpion ensured by the XML hierarchy. Thus, TEI-XML combines structure markup with analysis markups:

```xml
<div type="scene">
  <head>Scene 1</head>
  <stage type="entrance">Enter Fay</stage> 
  <sp who="#spFay">
    <speaker>Fay</speaker>
    <p>I say, Dinah, has anyone seen my gloves?</p>
  </sp>
  <stage type="entrance">Enter Dinah</stage>
  <sp who="#spDin">
    <speaker>Dinah</speaker>
    <p>No, miss, perhaps the parakeet has got them again?</p>
  </sp>
  <stage type="exit">Exit Fay and Dinah</stage>
</div>
```

In the example above, there are `<div>` and `<head>` markup that describes the structure of the document alonside the understanding markups `<sp>` that contains `<speaker>` and `<p>` markups subelements for speaker name and relative prose. TEI-XML specifications allow for a lot of combinations that are specially oriented for text study purposes. Therefore, the standardization has its limits and the panel of possible use is reduced to academics and text studies oriented document edition.

#### ALTO

Another option is to consider ALTO-xml format. As the TEI-XML format, ALTO was conceived around OCR issues.  Conversely to add analysis informations, it focuses on a standardized description of the data from the OCR tools and the relative layout. Consider the example below:

```xml
<TextBlock ID="page_9de728a2-2430-4730-aa63-f1aff036b714_textblock_5">
	<TextLine HPOS="2047" VPOS="838" WIDTH="78" HEIGHT="28" PROCESSINGREFS="worker_0a03b29d-b328-4866-a2cf-ada5bcd10f46">
		<Shape>
			<Polygon POINTS="2047,838 2047,866 2125,866 2125,838 2047,838"/>
		</Shape>
		<String CONTENT="Artists" PROCESSINGREFS="worker_85c58058-4ac8-48c1-bb35-9a5b92fa04a2"/>
	</TextLine>
</TextBlock>
```

The `<TextBlock>` markup indicates a text zone and allows a reference to the original page through the ID attributes alongside the ordering of the text block in the document. Each textline has the relevant boundingbox coordinates and references the worker involved in the recognition of the coordinates. In the textline element, we find the `<Shape>` markup subelement where the above worker DLA detected a polygon with its relative points coordinates. At the same level, the `<String>` markup has an attribute for the recognized character string and the worker OCR reference.
This structure ensures a large variety of use, among TEKLIA customers and therefore TEKLIA chose to work with ALTO when dealing with XML document generation. 

### XML on Python

As explained above, choice was made to work with ALTO format. Manually editing an XML file would be tedious, as inserting each markup and subelement with their attributes will lead to layout and syntax errors. Handling XML on Python requires to use an XML parser, a library design for parsing and generate XML files. We will discuss the choice between the two main XML parsers for Python, that is to say lxml and xml libraries. Both library shares the same ElementTree core method that generated xml elements.

#### lxml library, a third party module

The purpose of the lxml library is to extends ElementTree operations available in the built-in module. The lxml library advantages over xml standard library concern the parsing side. Using lxml allows parsing HTML documents that are not XML compliant and hence is used for web-scraping operations. The BeautifulSoup parser and engine in Pandas, pandas.read_html() relies on lxml. However, there is no use for parsing XML file as the aim is to create it. Moreover, as a third party module, lxml requires installation, that is to avoid when deploying our script. 

#### xml library

The xml library is a built-in module, even if it doesn't offer lxml functionalities the ElementTree method allows us to create the XML elements hierarchy. Once the root element is defined, all subelement can be created with their tags and attributes. Finally, the tree is generated and written on designated file. We will discuss technical points later, but for now as our need does not concern parsing, the built-in xml library is adequate.

## Technical approach

### A script of simple use

Both for internal and customer use, the whole multimedia-export script as to be simple to use. Launching python each time to get access to pdf or xml generation would be tedious. 

* The script should be launched directly from the terminal console, creating an terminal alias when installing the python script. This was achieved by configurating the setup.py file within the multimedia-export file directory then pushed on gitlab for continuous deployment.
* The script must accept several command line interface (CLI) arguments. This functionality is implemented in Python through the use of the argparse library.

```python
def parse_args():
    """
    Catches the database path from the CLI
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_database_path",
        type=Path,
        help="Path to the multimedia database",
    )
```
The example above catches a CLI argument that python code will handle as "input_database_path" that has to be of Path type.

* Python allows to create custom types through the definition of a class using Enum library. The mode option value can be later called as if it was an attribute of the arg parser object and accepts two possible input values, "pdf" or "alto"

```python
class Mode(Enum):
    """Predefines choices for mode CLI option"""

    pdf = "pdf"
    alto = "alto"
...
parser.add_argument(
        "--mode",
        default="pdf",
        type=Mode,
        choices=list(Mode),
        ...
```

An example of resulting CLI command when generating an alto xml file is:

```bash
arkindex-export ./smolpdf.sqlite --line-type text_line --mode alto --output ../repoperso --folder-type folder
```
assuming
```python
arg=arg_parse() 
```
arg.mode returns "alto"


### Where to find relevant data for the document generation: Arkindex-export

As a front-end oriented platform, Arkindex database was designed through Django. As many users can access to Arkinidex platform, all their personal informations for connecting Arkindex are embedded in Arkindex database. It is the kind of informations we don't need (and we dont't want), when processing data from the OCR and HTR data. Therefore, TEKLIA developers added a functionality to export Arkindex database, for user to get the machine learning oriented data. Here is the resulting Arkindex-export database model users will handle retrieving the export database.
![Arkindex-export database model](./structure.png)
It is important to remember that, as the Arkindex is a web platform, the database model is related to applicative needs and its structure is evolving with new needs and optimization.

* the "element" table is the table that would be the central one. Each element, is stored with an id, date of update and creation, its name and its type. 

```bash
sqlite> pragma table_info(element);
0|id|VARCHAR(37)|1||1
1|created|REAL|1||0
2|updated|REAL|1||0
3|name|VARCHAR(250)|1||0
4|type|VARCHAR(50)|1||0
5|polygon|TEXT|0||0
6|image_id|VARCHAR(37)|0||0
7|worker_version_id|VARCHAR(37)|0||0

```
If the element was detected by the OCR or HTR, it is linked to an image and thus it has a polygon. Actually, as the export database is for HTR or OCR purpose, all element have a polygon field and image field. This field is null if the element is linked to the structure of a project: A volume can contain several pages, each page would have a polygon and an image associated, but the volume type element itself would not.

* therefore, by default, each element would have an image, which adress is stored in the "image" table.

* the "transcription" table contains HTR or OCR processed results, its text corresponding to a sentence or a word. It has an element id associated with. Other informations are the confidence of the recognition and the id of the worker that ran the OCR or HWR model.

* the "worker_version" table gathers the informations about the workers who are in charge of launching HTR or OCR tool to analyse pools of images.

* the "element_path" table is set to store hierarchy between elements and the order between elements at the same level. For example, a folder would be the parent of several pages. By the way, pages will share the same folder parent_id and the folder will have several child_id that is to say the element id of each page. In element_path table, "ordering" value for pages will follow the order of pages in the document as all pages are at the same hierarchy levels.

Peut-être rajouter les tableaux. Element_path table d'association ?
Even if arkindex-export database contains more tables, informations needed for the pdf and TEI-XML generation reside in the above tables.

### A first SQLite basic query

As all the necessary data to generate the documents is already available in the export database, we can now discuss how to retrieve this data from it. Here is the basic query that we will reuse along the project. 

```python
def list_elements(db_as_string: str, type: str) -> List[Element]:
    """
    Gets a database path, and the specified page type
    """

    # connection to the database
    connection = sqlite3.Connection(db_as_string)
    cursor = connection.cursor()
    cursor.execute("SELECT id, name, polygon FROM element WHERE type = ?", (type,))
    result = list(starmap(Element, cursor.fetchall()))

    cursor.close()
    connection.close()

    return result
```

* as all the implementation goes through Python, instead of an SQL script, a query function was prefered. As information has to be retrieved regarding to the hierarchy of elements, the type of the element is passed as argument. Imagine a whole project with several folders containing several pages. Each document will have the name of the relative folder, pages will have to be gathered in the document with its relative lines.
For each element of a certain type, identifying it with its id, getting the name and get its polygon will be good enough for handle it. Further queries will treat more complex case.

### Reusing the basic query

It is to note that for more code readibility all query results will be returned as namedtuples. As the list_elements function returns values from the element table. We want the result to be returned as a renamed entity, from which we can easily retrieve features with more sense to the developer. Therefore, instead of getting a list of tuples from which a value must be called by an index we can call them as if we were calling an attribute. It is particularly useful when reusing the former query function, consider the following function that retrieved all elements from the folder type.

```python
def list_folders(db_as_string: str, folder_type: str) -> list:
    """
    Gets a database path, the optional folder_type as strings
    """

    return list_elements(db_as_string, folder_type)
```

Remember that the folder type name can vary, for some project the folder name would be "folder", for others it can be "volume". When looping from the list returned by this function, we can easily get a specified field. Here is an example:

```python
for folder in list_folders(...):
    print(folder.id)
```

Without namedtuples, the code would be less meaningful:

```python
for folder in list_folders(...):
    print(folder[0])
```

In the same way we want to get the list of folders for a given project, the "list_pages" function will retieve pages elements and "list_lines" will retrieve lines elements.

### SQLite recursive queries

Something is still missing when retrieving elements. We would like to get all the elements that come from the same parent element. For example, from a certain page we want to get all the paragraphs, and for all the paragraph we want to get all the line transcriptions. Selecting all elements where parent_id is the page_id will only return the paragraphs elements. Thus, to ensure we get all the childs from a parent, we have to set a recursive query as in the example below:

% Pas besoin de tout le code juste la requete

```SQL
        """
            WITH RECURSIVE child_ids (element_id) AS (
                    SELECT child_id
                    FROM element_path
                    WHERE parent_id = ?
                UNION
                    SELECT child_id
                    FROM element_path
                    JOIN child_ids ON (element_path.parent_id = child_ids.element_id)
            )
        SELECT transcription.id, transcription.element_id, text
        FROM transcription
        JOIN child_ids ON transcription.element_id = child_ids.element_id;
        """
```

This query select all values from the "transcription" table related to the given element id. It looks in the "element_path" table to get direct childs where given element id corresponds to a parent id. Recursive action also looks for herited child elements by replacing original parent id by the new selected child ids. 

### How to ensure correct working of functions

Another advantage for setting a query as Python function is to enable unit tests on it. Manually testing queries on certain values would not prevent from invalid results as not all cases might be taken into account. Best practice is to set a unit test environnement within our Python project. The chosen one relies on pytest and tox. Tox could be seen as an overlay for pytest easing automated testing within Gitlab Continuous Integration (CI) process. It specfies external modules libraries used by custom functions so that Gitlab can run the complete test environnement and that the tests report is being edited.

* the fixture: a query function cannot be tested using the whole database as it will be too large for cloud storage, memory consuming. Moreover, test cases have to be created from scratch to ensure that interaction with the database structure is working as expected. A smarter way to do so is to set a fixture that recreates the working of real database. Structure has not to be define as it is the same structure of the real databse. You can retrieve the original structure with:

```sqlite
sqlite> .open mydb.sqlite
sqlite> .schema
```

The sqlite code will be part of an SQL script recreating a blank database. Next step is to add values in order to verify values returned by the query functions with the INSERT INTO sql statement. This fixture consisting on running the sql script can be written as a function.

A réinvestir pour la certif

```python
@pytest.fixture
def export_db_path(tmp_path, fixtures_dir):

    db_path = tmp_path / "multimedia_export_test.db"

    connection = sqlite3.Connection(db_path)
    cursor = connection.cursor()

    script_path = fixtures_dir / "structure.sql"

    with open(script_path) as f:
        cursor.executescript(f.read())
    cursor.close()

    return db_path
```

 and then be called as argument in unit test functions.

```python
def test_list_elements(export_db_path, type, expected):
    """
    Tests correct values from parametrized list are returned by the list_pages
    function
    """
    assert list_elements(export_db_path, type) == expected
```

* parametrized tests: as you may have noticed, comparing returned values with expected ones is done in a shorter way. This is made possible by the use of paramtrized unit test. It increases readability and factorizing of code by creating a list containing both input parameters for the function to test and the relevant expected values. Here is an full unit test with parameters lists and relative unit test function:

%%%% Réduire le test paramétrisé

```python
@pytest.mark.parametrize(
    "page_element,expected",
    [
        (
            Element(
                "pageid1",
                "pagename1",
                "[[0, 0], [0, 219], [230, 219], [230, 0], [0, 0]]",
            ),
            (230, 219),
        ),
    ],
)
def test_image_draw(page_element, tmp_path, expected):
    """
    Tests correct starting coordinates, width and height is return from polygon
    coordinates
    """
    image_path = SAMPLES / "image.jpg"
    c = canvas.Canvas(tmp_path / "testpdf")
    assert image_draw(page_element, image_path, c, tmp_path) == expected
```

### The data pipeline

Faire un schéma avec google apps, avec les fonction image download, etc

### PDF generation steps

As mentioned above, the document generation part relies on both PIL and Reportlab library. PIl is used for image handling and Reportlab for editing the pdf document.

* getting an image: Once the image for a page have been downloaded through image_download, image is opened with PIL. The object returned has some useful attributes such as image dimensions. An image is always associated with a polygon detected by the HTR/OCR tool (the contrary would mean something gone wrong with the HTR/OCR tool or a value has been manually inserted directly into the database). Mettre l'image de la double page
* resize the image: it can necessary to resize the image to the polygon zone. Common case is when an image has actually two pages delimited by two polygons. Rajouter l'image avec une double page. PIL library includes the method .crop to do so.
* drawing image: as the new page area is now delimited it can be inserted in the document with Reporlab .drawImage method. As the new cropped image should be smaller than the initial image, the page has to be resize to fit with the cropped image.
* bounding box: most of the time, page polygon is to be a rectangle, but for line elements, polygon can be much more complicated. ![Complex polygon](./complexpolygon.png) 
To ease text insertion, the bounding box while created a new polygon that bounds the complex one. The new bounding box can be drawn with the Reportlab .rect method. 
* text line insertion: as text line is retrieved and can be linked to its polygon by its id for text line can be inserted at relative bounding box with .drawString Reportlab method. In first approach, the fontsize would set to the line box height, but it won't ensure the text fit the line width as in the example above.![Too long string](./longerstring.png)
* fit text with the line box: to overcome the problem of mismacthing text width, a simple solution would be to adapt the fontsize so that the entire string match with the line box width. Here is the approach that has been currently adopted:
Rajouter les étapes et le résultat

* making text invisible: in this final step, pdf_gen function is added a new argument to specify if the pdf document is a debug one or for an end-user. In user mode, the new argument will set fontcolor to transparent (peut-être rajouter comment ils ont fait la transparent color) so as the linebox. Resulting document only shows original images (possibly cropped) and allows for textual search showing relative transcriptions found by the HTR/OCR tool, as in the example below:![User pdf](./Finalpdf.png)

### XML files generation

ALTO is maintained by a large community participating to the evolution of this format. Each evolution of the format has its own specifications, preserving compatibility with older versions when possible.
In the latest specifications, a single source image is allowed for a given ALTO XML file. As the pages of the original document refer to a single image, the Python script would create one XML file per document page gathering all files in the same folder. Each XML file would be named by a page identifier. In the same way, each folder of the corpus in the export database would be named by folder identifier. Each XML file can be written by using the xml built-in module. The following steps must be achieved:

* registering namespaces:
XML format reserved element and attribute names are declared within namespace declaration. For example, xml element name is linked to the namespace http://www.w3.org/XML/1998/namespace.   
Similarly, ALTO format requires to define namespaces for specific element names and attributes, binding them to their relative reference. For example, the xsi element designates the xml schema instance and references to a predefined ALTO version schema. As this element is predefined and reserved in ALTO specifications, it is declared within its namespace. Using the xml library with Python, allow namespace declaration as in the exmaple below:

```python
# setting the namespaces from alto v4.2
        ns_dict = {
            "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
            "xmlns": "http://www.loc.gov/standards/alto/ns-v4#",
            "xsi:schemaLocation": (
                "http://www.loc.gov/standards/alto/ns-v4# "
                "http://www.loc.gov/standards/alto/v4/alto-4-2.xsd"
            ),
            "xmlns:xlink": "http://www.w3.org/1999/xlink",
        }
```

The xmlns namespace is defined by its own URI, and xmlns attributes associated with their own URI within xmlns.
The xml library requires namespace declaration within a dictionary, and the root element gets its attributes from the namespace:

```python
# defining root element of the alto doc as arkindex folder
            alto_root = ET.Element("alto", ns_dict)
```

The result is an ALTO xml file with a root element named alto and sharing the alto namespace as attributes.

```xml
<alto xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.loc.gov/standards/alto/ns-v4#" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.loc.gov/standards/alto/ns-v4# http://www.loc.gov/standards/alto/v4/alto-4-2.xsd">
```

ALTO file must fit the schema specified at the schemaLocation attribute value http://www.loc.gov/standards/alto/v4/alto-4-2.xsd. For this schema, the alto root element accepts 4 possible subelements:

```xml
<xsd:element name="alto" type="altoType">
...
</xsd:element>
<xsd:complexType name="altoType">
    <xsd:sequence>
    <xsd:element name="Description" type="DescriptionType" minOccurs="0">
    ...
    </xsd:element>
    <xsd:element name="Styles" type="StylesType" minOccurs="0">
    ...
    </xsd:element>
    <xsd:element name="Tags" type="TagsType" minOccurs="0">
    ...
    </xsd:element>
    <xsd:element name="Layout" type="LayoutType">

```

Each of them accepting specified subelement and attributes.

* validating xml:

As an xml file, the alto schema is reduced to the minimal information. It could be difficult to check if the generated xml file is an ALTO one. Consider the extract of the alto schema below:

```xml
<xsd:element name="sourceImageInformation" type="sourceImageInformationType" minOccurs="0"/>
```

minOccurs ="0" specifies sourceImageInformation is optional, as it can be omitted. On the contrary, the schema does not precise the maxOccurs value. It implicitly means that maxOccurs="1". So ALTO format accept just one source image information. A good pratice consists on validating an XML schema through a XML parser, as we mentioned above. The opportunity to automate such validations will be discuss later.
For now, priority was given to validate the chosen alto version. Online validators offer a quick way to test xml schema but maximum file size is often limited. Second option is to use the xml parsers such as xmllint. After installation of xmllint on your comuter, the following command results in validating or listing errors in yourfile.xml according to schema.xsd xml schema:

```
xmllint --schema schema.xsd yourfile.xml --noout
```

Compétences validations des données XML avec le parser voir les compétences.
There are several way to verfiy alto format, parler qu pas systématique voir quel lib fait le parsing et verfi selon un schéma.

XML lint


# Project implementation

## Team management

From the start TEKLIA wished to get a product that customer could be provided with and all the project management was intended for. Agile practices were used so that they would not became a constraint. Technical interpretation of final user needs had already been done. Daily scrums were replaced by meetings three times a week, moments to share experience on current tasks completion. Many times, Christopher Kermorvant, Marie-Laurence Bonhomme and Martin Maarand also gave me precious help as product owners. My internship tutor, Bastien Abadie was in charge of the project lead. He defined the consecutive steps to reach in agreement with product owners. Expectations evolved from basic tasks to more autonomy and initiatives as i was gaining confidence.
Internal working within TEKLIA team mainly relies on the KANBAN method, A pool of issues were set, according to their order of completion, and possibly their priority.  At his side, my technical mentor, Erwan Rouchet was reviewing my code and managing the project gitlab repository. Both were coaching me on development concepts and correcting my mistakes with exhaustive explanations. 

### Peer programming

As continuous integration and continuous deployment is still new for me and requires some dev ops skills, my technical mentor helped me configure all setup files for automated tests and continuous deployment. It eases the further use of flake for code linting and codespell for correcting spelling mistakes.

### Gitlab as project hub for team exchanges

Starting from the project gitlab repository, these were the steps to follow: 
* Issue was edited on gitlab by either Bastien, Erwan and possibly Martin. A summary of the expectations were followed by some examples.
* A new branch was created from the "main" branch specific to the issue, with an explicit name related to the branch name.
* Work was locally achieved with frequent commits and branch push. The commit name has to content explicit: it must be related to what the commit is about. It is particularly useful when starting a review or continuing a team project.
* Once work on a branch was considered as finished. A merge request was send, informing that code was ready for review. 
* Many times, the code has to be improved, and the reviewer can edit threads that implies and a change by the assigned developer.
* When all threads are closed, that is to say that problems seem to be resolved, a new request review is sent. If no new problem is detected the issue is closed, and a new issue started.
* Most of the time, the reviewer will not immediately review the code when the review request is sent. To continue working on next issue, a new branch is created from the current branch, and is commited alongside the main one. Once the branch waiting for is merged, the new issue branch needs to be updated with last changes from the main branch. Thus, this new branch has to be rebased:
Rebasing a branch consists on moving it to a new commit on another branch. However, Git is a versioned file system and the file content on both branches may differ with their relative commit. Possible conflicts must be resolved by validating changes from one or another branch (or from both).

### A first simplified sample

Teklia team decided that the project would start with a simplified database, resulting from the optical character regnognition of a simple pdf file. The database contained a single folder element with 5 dependants pages elements and their relative lines. It has been a suitable way to understand how an arkindex-export database works: 
* what are the role of each table and 
* what are the relationship between the different kind of elements. 
This incremental approach eased understanding complex cases encountered in real corpus document generation, being the aim of the project.

## Technical feedback

### Database export

The project was not based on conceiving the database structure as the database was already implemented in the arkindex platform, for applicative purpose. Thus, understanding the whole arkindex database structure would have been a tough task, as optical recogintion data, human user and worker data are gathered in. Database export for analysis purpose is clearly a time saving functionality.

### Systematical unit tests

The database export leads to a reduced database for analysis purposes removing all critical informations from all users.

* Understanding the database value types

Even though, the resulting database is still structured around the working of the OCR tool. Therefore, the data structure would not have been easy to understand just manipulating it. That's why the systematical unit tests on function querying the database have been a helpful approach on understanding it. Unit tests implies to test funtions not on the whole export database. It wouldn't ensure reliability of tested functions as it reprocuces normal conditions where function is supposed to be already working. Beyond this, it implies performance and continuous integration issues as the whole database is far larger than the test one. Thus, unit tests implied reproducing the whole structure but with new full custom values. It binds tester to understand datatype for each database table element and possibly adjust function to have a better behavior in normal cases. 

* Adding control on standard function

A significant example is with table elements identifiers. SQLite has no specific type for Universally unique identifier (UUID), so id fields are simply declared as a variable characters (VARCHAR). However, the idea of testing functions on arbitrary strings that do not respect UUID format shows that control over data is lost. In the whole database, each id must be written in the UUID format. So the choice as been made to add a control on functions to ensure values returned are of the relevant type. Therefore,  improvement:

```python
    filtered_list = []
    for elt in query_result:
        # must convert values from query result
        # (returned as strings by cursor.fetchall) in uuid type
        if uuid.UUID(elt[0]) in element_ids:
            filtered_list.append(elt)
    return filtered_list
```

A unit test led to code quality improvement: it led to add a control over values returned by the query verifiying ids are well formatted.

* Understanding table dependencies

Beyond single table structure, unit tests also obligate to understand table dependencies. Filling the database mock with a new value in a new field can lead to constraints errors.
Consider the element table structure:

```sql
CREATE TABLE element (
    id VARCHAR(37) NOT NULL,
    created REAL NOT NULL,
    updated REAL NOT NULL,
    name VARCHAR(250) NOT NULL,
    type VARCHAR(50) NOT NULL,
    polygon TEXT,
    image_id VARCHAR(37),
    worker_version_id VARCHAR(37),
    PRIMARY KEY (id),
    FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE,
    FOREIGN KEY (worker_version_id) REFERENCES worker_version (id) ON DELETE CASCADE
);
```

Adding an image_id or worker_id to its mock will raise an error if the relative value was not inserted first in the relevant image or worker_version table. Value insertion has to be done in the correct order starting from worker_version, image and finally element table. It leads to a better comprehension of the database and by the same way of the OCR tool working. Database is filled by running an OCR tool through a specified worker, and each detected element is linked to the image processed by the OCR tool.

### Using document generators

Another challenge of the project was to use both reportlab library for pdf generation alongside PIL image library for image edition.

* Reportlab

First noticeable fact is that reportlab has a user-oriented coordinates system by default.
Reporlab coordinates starts from left-bottom corner of the page to be edited. A point is represented by two coordinates. The first one corresponding to a number of pixels starting from the left of the page moving off to the right. Same thing for the second one, starting from the bottom of the page moving off to the up. Even the coordinates system is customisable, as origin point can be translated and axis can be reverted. It is clearly the best coordinates system choice at first as it is the usual one. End users as well as developers can easily identify elements position with it.
The fact is that reportlab coordinates system is different from arkindex and PIL one. Arkindex, as well as PIL, has coordinates starting from top-left corner leading to a reverted vertical axis. To keep a system that makes sense for an end-user and for debug purpose, choice has been made to convert Arkindex coordinates into reportlab ones. 
This end-user approach has some counterparts as explained below:

```python
def bounding_box(polygon) -> Optional[BoundingBox]:
    """
    Gets the coordinates of the a polygon and sets the coordinates of the lower
    left point at which box starts, third value is the width of the box, the last
    one is the height,
    the y axis is switched in arkindex coordinates, starting from top left corner
    to the bottom whereas for reportlab the y axis starts from bottom left corner
    to the top,
    implies reportlab first point corresponds to arkindex (min_x,max_y)
    """
    ...
    return BoundingBox(min_x, max_y, width, height)
```

As Arkindex y-axis is inverted, when creating a bounding box for an element, left-bottom point correponds to the minimal x and maximal y values retrieved from Arkindex coordinates. For further processings, the bounding box coordinates have to be converted into reportlab ones as they are still expressed as Arkindex ones.

```python
# drawing line polygon bounding box
                c.rect(
                    line_box_dim.x,
                    image_height - line_box_dim.y,
                    line_box_dim.width,
                    line_box_dim.height,
                    # linebox visible according to debug value
                    stroke=debug,
                )
```

When drawing a bounding box through canvas.rect reportlab method, point where to start drawing the rectangle has to be specified, as the point is retrieved from Arkindex coordinates, x coordinate does not change but y coordinate must be converted into a reporlab one. From the top of the page at image_height the arkindex y coordinate has to be substracted to get the relative reportlab coordinate.
Even though this process seems to be tough, it is a simple way to avoid other approaches with their counterparts:

* first alternative would be switching between the two coordinates systems all along function process but it would increase code length,
* second alternative would be to set reportlab coordinates to match Arkindex one but would result in more difficulties with handling reportlab method as Arkindex coordinate system is not the usal one.

## Project feedback

### Development good practices

Working on TEKLIA allowed me to get the good practices when contributing to a professional project. Thanks to the reviews done on my work, i have learnt the basic rules to get a code quickly understandanble by a team contributor:

#### Explicit code and declaration

So that the reviewer get a first overview of code structure, pep recommendations makes sense when creating a function.

* The docstring description set the aim of the function and helps the reviewer validating function works correctly.
* Even if typed variables declaration has no consequence when executing code it is a suitable information for easing code review.

#### Factorising code

Factorising code is time-saving practice. 
* In that sense Python has a lot of implicit rules that permit a lot of code shortcuts.
* At the contrary, developer code must keep the critical information explicit for the reviewer.
* Avoid any type of code that can be rewrote shorter keeping the sense and aim of the
* The code must follow the structure conceived by the developer: verbose code can be the sign of that the relative structure must be improved in its conception.
* Secondary cases must be cleared out first in order to keep code reader focused on algorythm accuracy on the regular cases.
* using suitable Python functionalities and library 

At a higher level when the code himself is not enough to understand an algorithm, then comments must help for comprehension.

## Experienced difficulties

* code quality has been a constant consideration, as it overlayed algorithm conception contributing to its refinment. Several times, i did not take advantage all Python functionalities to factorize code as Python remains a new language for me.

* using gitlab in a professionnal context was also a new practice for me as I former used only basic back-up functionalities. It took me time to digest the merging process, implying the rebase step before to start a new issue. The asynchroneous work on several issues hardened the rebase process but trained me on real cases reinforcing my understanding on git concepts.

## What is left to be improve

### Refactoring code is to be improved at many levels

* For now, several queries have been written to retrieve elements of different types. The next step is write a new query that does the same work of the former queries. 
* As the console script has reached the deployment step, the next step is to embed pdf and alto generation on arkindex platform. It would implies to continue the integration tests on the whole console script.
* At a higher level, the whole code could be written on a object-oriented approach. It allows gathering all functions with common references into the class. It would avoid common code lines through the different functions. For example, all the functions that query the database could be gathered in the same class as in the example below.

```python
class Database(object):
    def __init__(self, connection_string):
        self.connection = sqlite3.connect(connection_string)
    def list_elements(self, folder_id):
        with self.connection.cursor() as c:
            ....
def main():
    db = Database(…)
    db.list_elements(folder_id=42)
```

# Conclusion.

I was really pleased to work within TEKLIA team, participating to this challenging project. It is very satisfying to know that the main aims have been reached and the document generation is now to be test on TEKLIA customers use. I felt very pleased to work on innovative solutions that fullfil real needs.
I would like to thank all the TEKLIA team for the time spent on explanations and their will to share their experience as machine learning and development experts!