<!---
Retrouvez mes projets sur [:fontawesome-brands-gitlab: GitLab](https://gitlab.com/?personal=true&sort=latest_activity_desc).
--->

## [NutriScoreApp](https://gitlab.com/simplon-dev-ia/grenoble-2021-2022/projets/projet-1/projet-1-groupe-4){target=_blank}

> Janvier 2021
<p style="text-align:justify;">
L'équipe IA de l'association Open Food Facts fait appel à vous !
Beaucoup de produits de leur base de données sont déjà annotés avec l'indicateur
nutri-score, mais il en manque encore. Ils se demandent si cet indicateur ne pourrait pas être inféré automatiquement
à partir des autres méta-données présentes sur chaque produit.
Vous êtes en charge de développer un prototype d'application web permettant de prédire le nutri-score
d'un produit à partir d'une série de critères à définir. L'application devra à minima proposer un formulaire
mais il est envisagé de développer une API REST permettant d'automatiser le processus.
</p>

*Compétences* :  
Mise en place d'un pipeline ETL  
Choix, entrainement, validation et inférence d'un modèle de Machine Learning  
Abstraction de la couche fonctionnelle d'un modèle de Machine Learning  
Mise en place d'une application web avec formulaire simple  

*Technologies* :  
Analyse de données : Jupyter, Pandas, Matplotlib, Seaborn  
Machine Learning : Scikit-Learn  
Web : Flask  

## [Job Dashboard](https://gitlab.com/Gabriel_CAFERRA/projet_3){target=_blank}

> Février 2021
<p style="text-align:justify;">
L'équipe des ressources humaines de Simplon cherche à comprendre les spécificités de l'emploi dans le secteur de la Data, afin de pouvoir guider au mieux les apprenants dans leur insertion professionnelle. 
  
Elle a retenu les besoins suivants:

</p>

* Une analyse journalière des offres d'emploi en ligne est un bon point de départ pour obtenir des données concrètes et à jour.

* Un système permettant de récupérer et d'analyser automatiquement les offres d'emploi dans le secteur de la Data, avec un tableau de bord présentant des métriques métier intéressantes sous la forme d'une page web accessible en ligne.

*Compétences* :  
Extraction de données (Scraping, flux RSS/Atom)  
Déploiement (GitLab Pages)  
Gestion de projet en mode Agile

*Technologies* :  
PostgreSQL, Python (environnement virtuel), SQL, Bash

## [Suivi de la pollution atmosphérique](https://gitlab.com/Gabriel_CAFERRA/projet_2-groupe_6/-/tree/projet_2){target=_blank}

> Janvier-Février 2021
<p style="text-align:justify;">
Les seuils fixés par l'Europe pour les particules PM10 et le dioxyde d'azote NO2 doivent être respectés sous peine de lourdes amendes.
</p>
<p style="text-align:justify;">
En Auvergne-Rhône-Alpes, différentes zones sont concernées par ces dépassements. Noux avons donc besoin de différents outils pour suivre et analyser la pollution atmosphérique à l'échelle de la région.

Quatre besoins ont été retenus :

</p>

* Avoir un tableau de bord avec différents indicateurs globaux (échelle de la région) concernant la qualité de l'air.  

* Avoir une cartographie de la région avec les différents polluants atmosphériques mise à jour toutes les heures.

* Avoir un notebook Jupyter qui contienne toutes les explications pour faire des requêtes sur la base de données ainsi qu'une cartographie. Le but est que les data scientists soient très vite opérationnels sur les données.

* Connaître l'impact du confinement sur la pollution atmosphérique dans la région.

*Compétences* :  
Développement web  
Intégration continue  
Déploiement continu  
Gestion de projet

*Technologies* :  
Git, Bash, SQL, PostgreSQL, Python, Metabase

## [Mesure d'audience Web](https://gitlab.com/Gabriel_CAFERRA/projet_1-groupe_2/-/tree/Gabriel-CAFERRA){target=_blank}

> Janvier 2021
<p style="text-align:justify;">
Nous avons récupéré les logs d'un serveur d'un site-web de e-commerce. Nous souhaitons pouvoir effectuer une mesure d'audience des visiteurs.

Nous avons pu répondre les questions suivantes :

</p>

* Quel est le traffic sur le site ?

* Combien de personnes visitent le site ?

* Quels sont nos produits les plus et moins intéressants pour notre clientèle ?

* De quel endroit d'internet viennent les visiteurs avant d'arriver sur le site ?

* Quels moments de la journée sont les plus/moins propices à la vente ?

* Quels navigateurs sont les plus / moins utilisés par les visiteurs ?

* Quels systèmes d'exploitation sont les plus / moins utilisés par les visiteurs ?

* Quels appareils sont les plus / moins utilisés par les visiteurs ?

*Compétences* :  
Conception, structuration, nettoyage de base de données relationnelle,  
Conception et réalisation d'un rendu visuel des données issues du processus d'extraction

*Technologies* :  
Git, Bash, SQL, SQLite, Python, Metabase
