## [Simplon Développeur Data](https://simplon.co/formation/specialisation-analyste-data/487){target=_blank}

> Novembre 2020 - Septembre 2021, Grenoble

<p style="text-align:justify;">

De l’analyse du besoin à la data visualisation, en passant par la récolte et le traitement des données, le·la développeur·se data conçoit et exploite les bases de données.
</p>
<p style="text-align:justify;">

Le·la développeur·se data gère l'ensemble du cycle de vie de la donnée, de la donnée brute jusqu'à la livraison de données utilisables. Il s’agit d'un·e technicien·ne capable d'appréhender n'importe quel type de format de données, de les stocker en base de données, les interroger et de les servir, avec un rendu visuel ou un support adapté pour un usage tiers. Il·elle peut être amené·e à automatiser des processus d'acquisition, d'import, d'extraction et de visualisation de données. Il·elle est le garant de la qualité, de l'intégrité et de la cohérence des données avant et après traitement.
</p>

Le métier de développeur·se data s’articule autour de 3 compétences que j'ai validées à l'issue de la formation :

* [Exploiter une  base de donnée](https://www.francecompetences.fr/recherche/rs/3508/)

* [Développer une base de données](https://www.francecompetences.fr/recherche/rs/3497/)

* [Méthodes agiles et amorçage de projets](https://www.francecompetences.fr/recherche/rs/2085/)

## [MASTER professionnel en Sciences Actuarielle et Financière ISFA](https://isfa.univ-lyon1.fr/cursus-formation-actuaire-a-lisfa){target=_blank}

> Septembre 2006 - Septembre 2008

<p style="text-align:justify;"> 

L'actuaire est un spécialiste de l'application des probabilités, des statistiques et de la gestion des risques. Il oeuvre dans les domaines de l'assurance, la finance et la protection sociale en apportant des réponses concrètes en termes de valorisation de portefeuilles, tarification, modélisation des risques, mesure de solvabilité, ...

</p>